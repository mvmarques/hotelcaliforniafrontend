import React from 'react';
import './App.css';
import logo from './assets/logo_m2.png';
import Routes from './routes'

function App() {

  return (
   <div >
     <div style={{ flexDirection: "row", width: '100%', margin : '0 auto', display: 'inline-block' }} >
       <img src={logo} style={{width: 300, height: 100, margin: 'auto', display: 'block' } } alt="AirCnC"/>
    </div>
    <div >
      <Routes />
    </div>

   </div>
  );
}

export default App;
