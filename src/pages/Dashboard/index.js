import React, { useEffect, useState, useMemo } from 'react';
import api from '../../services/api';
import './styles.css';
import { Link } from 'react-router-dom';
import {NavLink} from "react-router-dom";
import { request } from 'https';
import { callbackify } from 'util';
import checkin from '../../assets/checkin.png';
import cadastrar_hospede from '../../assets/cadastrar-hospede.jpg'
import hospede from '../../assets/hospede.jpg'
export default function Dashboard() {

    const [ spots, setSpots ] = useState([]);
    const [ requests, setRequests ] = useState([]);

    async function handleAccept(id) {
        // await api.post(`/bookings/${id}/approvals`);

        // setRequests(requests.filter(request => request._id !== id));
    }

    async function handleReject(id) {
        // await api.post(`/bookings/${id}/rejections`);

        // setRequests(requests.filter(request => request._id !== id));
    }

    function navigateTo(path) {
        
    }

    return (
        <div className="dashboard-content" >

        <h1 style={{ textAlign: 'center'}}>DASHBOARD</h1>
          
               
                    <div style={{ flexDirection: 'row', display: 'flex' }}>
                    
                        <div style={{
                                margin: '1em', 
                                width: 'calc(100% - 50em)',
                                height: 'calc(100vh - 30em)',
                                borderRadius: '7px',
                                background: '#F63145',
                                overflow : 'auto'
                        }}>
                            <img src={checkin} style={{
                                //width: '260px',
                                height: 'inherit'
                            }} alt="AirCnC"/>
                      
                        </div>
                        
                       
                        <div style={{
                                margin: '1em', 
                                width: 'calc(100% - 50em)',
                                height: 'calc(100vh - 30em)',
                                borderRadius: '7px',
                                background: '#F63145',
                                overflow : 'auto'
                        }}>
                            <img src={cadastrar_hospede} style={{
                               // width: '260px',
                                height: 'inherit'
                            }} alt="AirCnC"/>
                        </div>

                        <div style={{
                                margin: '1em', 
                                width: 'calc(100% - 50em)',
                                height: 'calc(100vh - 30em)',
                                borderRadius: '7px',
                                background: '#F63145',
                                overflow : 'auto'
                        }}>
                            <img src={hospede} style={{
                             //   width: '260px',
                                height: 'inherit'
                            }} alt="AirCnC"/>
                        </div>

                        {/* <div style={{
                                margin: '1em', 
                                width: 'calc(100% - 50em)',
                                height: 'calc(100vh - 30em)',
                                borderRadius: '7px',
                                background: '#F63145'
                        }}></div> */}

                    
                    </div>

               <div style={{ flexDirection: 'row', display: 'flex', width: '100%', height: 'calc(100vh - 43em)' }} >
                    
                    <button className="reserva-btn" style={{ marginLeft: '3em' }} component={Link} to="/">
                        <Link to='/cliente' >CLIENTES</Link>
                    </button>
                    
                    <button className="reserva-btn"  style={{ marginLeft: '10em' }} >
                        <Link to='/quartos' >QUARTOS</Link>
                    </button>
                    <button className="reserva-btn"  style={{ marginLeft: '10em' }} >
                        <Link to='/reserva' >RESERVA</Link>
                    </button>
               </div>
        </div>
    )
}