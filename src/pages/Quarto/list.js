
import './style.css';
import React, { useEffect, useState, useMemo } from 'react';
import api from '../../services/api';
import { Link } from 'react-router-dom';
import { NavLink } from "react-router-dom";
import DataTable from 'react-data-table-component';


const columns = [
    {
        name: 'Categoria',
        selector: 'categoria',
        sortable: true,
    },
    {
        name: 'Estrelas',
        selector: 'classificacao',
        sortable: true,
        right: true,
    },
    {
        name: 'Numero',
        selector: 'numero',
        sortable: true,
        right: true,
    },
    {
        name: 'Andar',
        selector: 'andar',
        sortable: true,
        right: true,
    },
    {
        name: 'Preço',
        selector: 'preco',
        sortable: true,
        right: true,
    },
    {
        name: 'Reservado',
        selector: 'reserva',
        sortable: true,
        right: true,
    },
];
export default function QuartoList({history}) {

    const [data, setData] = useState();
    const [selected, setSelected] = useState('');

    useEffect(() => {
        getRooms();
    }, []);

    async function getRooms() {
        const retorno = await api.get('/quartos');
        await retorno.data.map( x => {
            if(x.reserva) {
                x.reserva = 'RESERVADO'
            }
        })
    
        setData(retorno.data)
    }

    const handleChange = (state) => {
        // You can use setState or dispatch with something like Redux so we can use the retrieved data
        setSelected(state)
        console.log('Selected Rows: ', state.selectedRows);
    };

    const exibidor = (state) => {
        setSelected(state.selectedRows)
        console.log(state.selectedRows)
    }

   async function excluir (state) {
       // setSelected(state.selectedRows)
       console.log(selected[0].id)
        //await api.delete('/quarto/delete', {id: selected[0].id});
        await api.post('/quarto/delete',{id: selected[0].id}).then( () => {  
            setData([])
            getRooms()
        }
        )

    }

    return (
        <div className="dashboard-cont" >
        <h1>Listagem geral</h1>
            <DataTable
                title="Quartos"
                columns={columns}
                data={data}
                selectableRows // add for checkbox selection
                Clicked
                Selected={handleChange}
                onRowSelected={exibidor}
            />

            <div style={{ display: 'inline-block' }}>
                <div style={{ display: 'flex' }}>
                    <button className="btni" style={{ marginLeft: '1em' }} >
                        <Link to='/quartos' >VOLTAR</Link>
                    </button>

                    <button className="btni" style={{ marginLeft: '1em' }} type='button' onClick={() => excluir()} >
                        EXCLUIR
                    </button>
                </div>
            </div>
        </div>
    )

}