import React, { useEffect, useState, useMemo } from 'react';
import api from '../../services/api';
import { Link } from 'react-router-dom';
import { NavLink } from "react-router-dom";
import { request } from 'https';
import { callbackify } from 'util';
import checkin from '../../assets/checkin.png';
import cadastrar_hospede from '../../assets/cadastrar-hospede.jpg'
import hospede from '../../assets/hospede.jpg'
import './style.css';
import camera from '../../assets/camera.svg'
export default function Quarto({ history }) {

    const [categoria, setCategoria] = useState('');
    const [classificacao, setClassificacao] = useState('');
    const [andar, setAndar] = useState('');
    const [numero, setNumero] = useState('');
    const [thumbnail, setThumbnail] = useState('');

    const preview = useMemo(
        () => {
            return thumbnail ? URL.createObjectURL(thumbnail) : null;
        }, [thumbnail]
    );

    //  function handleSubmit(event) {
    //     event.preventDefault();
    //     const data = new FormData();
    //     const user_id = localStorage.getItem('user');

    //     data.append('thumbnail', thumbnail);
    //     data.append('company', company);
    //     data.append('techs', techs);
    //     data.append('price', price);

    //     await api.post('/spots', data, {
    //         headers: { user_id }
    //     })

    //     history.push('/dashboard');
    // }

    async function saveData(event) {

        event.preventDefault();

        await api.post('/quarto/salvar', {
            categoria,
            classificacao,
            numero,
            andar,
        }).then(x => {
            alert(`Quarto do tipo ${x.data.categoria} registrado com sucesso`);
            console.log(x);
            history.push('/dashboard');
        }
        )

        //  console.log( categoria, classificacao, numero, andar)

    }


    return (
        <div className="dashboard-content" >
            <form onSubmit={saveData}>
                <div style={{ flexDirection: 'column', display: 'inline-flex' }}>
                    <div style={{
                        width: '58em',
                        height: '10em',
                        background: '#dad8dc',
                        borderColor: '#e02828',
                        borderStyle: 'dashed',
                        borderRadius: '3px'
                    }}></div>

                    {/* <label id="thumbnail" 
                                style={{ backgroundImage: `url(${preview})`}}
                                className={thumbnail ? 'has-thumbnail' : ''}
                                >
                            <input type="file" onChange={event => setThumbnail(event.target.files[0])}/>
                            <img src={camera} alt="Select img"/>
                        </label> */}
                </div>




                <div style={{ display: 'row', width: 'fit-content' }}>
                    <div style={{ flexDirection: 'column', display: 'inline-flex' }}>
                        <label htmlFor="cat" style={{ margintop: '1em', textTransform: 'uppercase' }} >CATEGORIA * </label>
                        <input
                            id="cat"
                            placeholder="Solteiro, Duplo ou Familia"
                            value={categoria}
                            onChange={event => setCategoria(event.target.value)}
                        />
                    </div>

                    <div style={{ flexDirection: 'column', display: 'inline-flex', margin: '1em' }}>
                        <label htmlFor="cla" style={{ margintop: '1em', textTransform: 'uppercase' }} >CLASSIFICAÇÃO * </label>
                        <input
                            id="cla"
                            placeholder="1, 2, 3, 4 ou 5 Estrelas"
                            value={classificacao}
                            onChange={event => setClassificacao(event.target.value)}
                        />
                    </div>


                </div>

                <div style={{ display: 'row', width: 'fit-content' }}>
                    <div style={{ flexDirection: 'column', display: 'inline-flex' }}>
                        <label htmlFor="and" style={{ margintop: '1em' }} >ANDAR * </label>
                        <input
                            id="and"
                            placeholder="1, 2, 3, ..."
                            value={andar}
                            onChange={event => setAndar(event.target.value)}
                        />
                    </div>

                    <div style={{ flexDirection: 'column', display: 'inline-flex', margin: '1em' }}>
                        <label htmlFor="num" style={{ margintop: '1em' }} >NUMERO * </label>
                        <input
                            id="num"
                            placeholder=""
                            value={numero}
                            onChange={event => setNumero(event.target.value)}
                        />
                    </div>
                </div>

                <div style={{ display: 'row' }}>
                    <button className="btn" type="submit">SALVAR</button>

                    <button className="btn" style={{ marginLeft: '1em' }} type='button' >
                        <Link to='/quartos-list' style={{ display: 'block', color: '#fff' }} >LISTAGEM</Link>
                    </button>

                    <button className="btn" style={{ marginLeft: '1em' }} type='button' >
                        <Link to='/dashboard' style={{ display: 'block', color: '#fff' }} >VOLTAR</Link>
                    </button>
                </div>


            </form>



        </div>


    )

}