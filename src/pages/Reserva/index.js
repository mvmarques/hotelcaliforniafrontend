import React, { useEffect, useState, useMemo } from 'react';
import api from '../../services/api';
import { Link } from 'react-router-dom';
import { NavLink } from "react-router-dom";
import { request } from 'https';
import { callbackify } from 'util';
import checkin from '../../assets/checkin.png';
import cadastrar_hospede from '../../assets/cadastrar-hospede.jpg'
import hospede from '../../assets/hospede.jpg'
import './style.css';
import Select from 'react-select'

import DataTable from 'react-data-table-component';

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' }
]
const columns = [
    {
        name: 'Categoria',
        selector: 'categoria',
        sortable: true,
    },
    {
        name: 'Estrelas',
        selector: 'classificacao',
        sortable: true,
        right: true,
    },
    {
        name: 'Numero',
        selector: 'numero',
        sortable: true,
        right: true,
    },
    {
        name: 'Andar',
        selector: 'andar',
        sortable: true,
        right: true,
    },
    {
        name: 'Preço',
        selector: 'preco',
        sortable: true,
        right: true,
    },
];

var options_returned = []

export default function Reserva({ history }) {

    const [data, setData] = useState();
    const [selected, setSelected] = useState('');
    const [dataGuest, setDataGuest] = useState([]);
    const [selectedGuest, setSelectedDataGuest] = useState('');

    useEffect(() => {
        getRooms();
        getGuest();
        
    }, []);

    async function getRooms() {
        const oi = await api.get('/reserva/disponiveis');
        const retorno = await api.get('/reserva/disponiveis');
        setData(retorno.data)
        console.log(retorno.data)
        console.warn(oi.data)
    }

    async function getGuest() {
        options_returned = []
           if(dataGuest.length === 0) {
             await api.get('/hospede').then(element => {
                 console.log(element.data);
                 element.data.forEach( arrReturned => {
                      options_returned.push({ value: arrReturned._id, label: `Sr(a). ${arrReturned.sobrenome}, ${arrReturned.nome}` })
                      setDataGuest(options_returned); 
                 });
                
            });
           }


    }

    const handleChange = (state) => {
        // You can use setState or dispatch with something like Redux so we can use the retrieved data
        setSelected(state)
        console.log('Selected Rows: ', state.selectedRows);
    };

    const exibidor = (state) => {
        setSelected(state.selectedRows)
        console.log(state.selectedRows)
    }

    async function voltar(state) {
        history.push('/dashboard')
        
    }

    async function reservar(state) {

        console.log(selected[0].id)
        console.log(selectedGuest.value)
        await api.post('/reserva/salvar', { checkin: new Date(), checkout: new Date(), hospede_id: selectedGuest.value, quarto_id: selected[0].id }).then(x => {
            
            alert(`Reserva realizada`)

            history.push('/dashboard')
        })

    }

    async function listar(state) {

    }

    const handleChangeSelect = (selectedGuest) => {
        setSelectedDataGuest(selectedGuest);
      }

    return (
        <div className="dashboard-cont" >
            <h1>CRIAR RESERVA</h1>
            <Select options={dataGuest} 
                    height='20px'
                    value={selectedGuest}
                    onChange={handleChangeSelect}
                    />

            <DataTable
                title="Quartos disponiveis"
                columns={columns}
                data={data}
                selectableRows // add for checkbox selection
                Clicked
                Selected={handleChange}
                onRowSelected={exibidor}
            />

            <div style={{ display: 'inline-block' }}>
                <div style={{ display: 'flex' }}>
                    <button className="btni" style={{ marginLeft: '1em' }} type='button' onClick={() => voltar()} >
                        VOLTAR
                </button>

                    <button className="btni" style={{ marginLeft: '1em' }} type='button' onClick={() => reservar()} >
                        RESERVAR
                    </button>

                    <button className="btni" style={{ marginLeft: '1em' }} type='button' onClick={() => listar()}>
                        <Link to='/reserva-list' style={{ display: 'block', color: '#fff' }} >LISTAGEM</Link>
                    </button>

                </div>
            </div>
        </div>
    );
}