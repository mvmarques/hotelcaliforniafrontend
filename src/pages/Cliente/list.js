
import './style.css';
import React, { useEffect, useState, useMemo } from 'react';
import api from '../../services/api';
import { Link } from 'react-router-dom';
import { NavLink } from "react-router-dom";
import DataTable from 'react-data-table-component';


const columns = [
    {
        name: 'Nome',
        selector: 'nome',
        sortable: true,
    },
    {
        name: 'Sobrenome',
        selector: 'sobrenome',
        sortable: true,
        right: true,
    },
    {
        name: 'E-mail',
        selector: 'email',
        sortable: true,
        right: true,
    },
    {
        name: 'CPF',
        selector: 'cpf',
        sortable: true,
        right: true,
    },
    {
        name: 'CPF',
        selector: 'cpf',
        sortable: true,
        right: true,
    },
    {
        name: 'CEP',
        selector: 'endereco.cep',
        sortable: true,
        right: true,
    }
];
export default function ClienteList({history}) {

    const [data, setData] = useState();
    const [selected, setSelected] = useState('');

    useEffect(() => {
        getClients();
    }, []);

    async function getClients() {
        const retorno = await api.get('/hospede');
        console.log(retorno.data)
        setData(retorno.data)
    }

    const handleChange = (state) => {
        // You can use setState or dispatch with something like Redux so we can use the retrieved data
        setSelected(state)
        console.log('Selected Rows: ', state.selectedRows);
    };

    const exibidor = (state) => {
        setSelected(state.selectedRows)
        console.log(state.selectedRows)
    }

   async function excluir (state) {
       // setSelected(state.selectedRows)
       console.log(selected[0]._id)
        //await api.delete('/quarto/delete', {id: selected[0].id});
        await api.post('/hospede/deletar',{id: selected[0]._id}).then( () => {  
            setData([]);
            getClients();
        })
    }

    async function editar (state) {
        console.log(selected[0]._id)
       //  await api.post('/hospede/editar',{id: selected[0]._id}).then( () => {  
            history.push('/cliente', { id: selected[0]._id, 
                                       nome: selected[0].nome, 
                                       sobrenome: selected[0].sobrenome,
                                       idade: selected[0].idade,
                                       cep: selected[0].endereco.cep,
                                       cpf: selected[0].cpf,
                                       email: selected[0].email
                                      })
       //  })
     }

    return (
        <div className="dashboard-cont" >
        <h1>Listagem geral</h1>
            <DataTable
                title="Clientes"
                columns={columns}
                data={data}
                selectableRows // add for checkbox selection
                Clicked
                Selected={handleChange}
                onRowSelected={exibidor}
            />

            <div style={{ display: 'inline-block' }}>
                <div style={{ display: 'flex' }}>
                    <button className="btni" style={{ marginLeft: '1em' }} >
                        <Link to='/cliente' >VOLTAR</Link>
                    </button>

                    <button className="btni" style={{ marginLeft: '1em' }} type='button' onClick={() => editar()} >
                        EDITAR
                    </button>

                    <button className="btni" style={{ marginLeft: '1em' }} type='button' onClick={() => excluir()} >
                        EXCLUIR
                    </button>
                </div>
            </div>
        </div>
    )

}