import React, { useEffect, useState, useMemo } from 'react';
import api from '../../services/api';
import { Link } from 'react-router-dom';
import { NavLink } from "react-router-dom";
import { request } from 'https';
import { callbackify } from 'util';
import checkin from '../../assets/checkin.png';
import cadastrar_hospede from '../../assets/cadastrar-hospede.jpg'
import hospede from '../../assets/hospede.jpg'
import cepify from 'cep-promise'
export default function Cliente({ history }) {

    const [nome, setNome] = useState('');
    const [sobrenome, setSobrenome] = useState('');
    const [idade, setIdade] = useState('');
    const [email, setEmail] = useState('');
    const [cpf, setCpf] = useState('');
    const [id, setId] = useState('');

    const [cep, setCep] = useState('');
    const [rua, setRua] = useState('');
    const [estado, setEstado] = useState('');
    const [cidade, setCidade] = useState('');

    useEffect(() => {
        console.log(history.location.state)
        if (history.location.state) {
            const { nome, sobrenome, idade, cpf, cep, id, email } = history.location.state;
            setNome(nome);
            setSobrenome(sobrenome);
            setIdade(idade);
            setCpf(cpf);
            setCep(cep);
            setEmail(email);
            setId(id);
            cepify(cep).then(x => {
                setRua(x.street)
                setCidade(x.city)
                setEstado(x.state)
            })
        }
    }, []);


    async function saveData(event) {
        event.preventDefault();

        if (!id) {
            await api.post('/hospede/salvar', {
                nome,
                sobrenome,
                idade,
                email,
                cpf,
                cep,
                rua,
                estado,
                cidade
            }).then(x => {
                history.push('/dashboard')
                alert('Hospede registrado com sucesso');
                console.log(x)
            })
        } else {
            console.log(cep)
            await api.put('/hospede/editar', {
                id,
                nome,
                sobrenome,
                idade,
                email,
                cpf,
                cep,
                rua,
                estado,
                cidade
            }).then(() => {
                history.push('/dashboard')
                alert('Hospede registrado com sucesso');
               // console.log(x)
            });
        }
    }

   async function _onBlur() {
           cepify(cep).then( x => { 
                setRua(x.street)
                setCidade(x.city)
                setEstado(x.state)
            }
        )
    }

    return (
        <div className="dashboard-content" >
            <form onSubmit={saveData}>

                <div style={{ display: 'row', width: 'fit-content' }}>
                    <div style={{ flexDirection: 'column', display: 'inline-flex' }}>
                        <label htmlFor="cat" style={{ margintop: '1em' }} > NOME * </label>
                        <input
                            id="cat"
                            placeholder=""
                            value={nome}
                            onChange={event => setNome(event.target.value)}
                        />
                    </div>

                    <div style={{ flexDirection: 'column', display: 'inline-flex', margin: '1em' }}>
                        <label htmlFor="cla" style={{ margintop: '1em' }} > SOBRENOME * </label>
                        <input
                            id="cla"
                            placeholder=""
                            value={sobrenome}
                            onChange={event => setSobrenome(event.target.value)}
                        />
                    </div>


                </div>

                <div style={{ display: 'row', width: 'fit-content' }}>
                    <div style={{ flexDirection: 'column', display: 'inline-flex' }}>
                        <label htmlFor="and" style={{ margintop: '1em' }} > IDADE * </label>
                        <input
                            id="and"
                            placeholder=""
                            value={idade}
                            onChange={event => setIdade(event.target.value)}
                        />
                    </div>

                    <div style={{ flexDirection: 'column', display: 'inline-flex', margin: '1em' }}>
                        <label htmlFor="num" style={{ margintop: '1em' }} > EMAIL * </label>
                        <input
                            id="num"
                            placeholder=""
                            value={email}
                            onChange={event => setEmail(event.target.value)}
                        />
                    </div>


                    <div style={{ display: 'row', width: 'fit-content' }}>
                        <div style={{ flexDirection: 'column', display: 'inline-flex' }}>
                            <label htmlFor="and" style={{ margintop: '1em' }} > CEP * </label>
                            <input
                                id="cep"
                                placeholder="Adicione um C.E.P real"
                                value={cep}
                                onChange={event => setCep(event.target.value)}
                                onBlur={_onBlur}
                            />
                        </div>

                        <div style={{ flexDirection: 'column', display: 'inline-flex', margin: '1em' }}>
                            <label htmlFor="cpf" style={{ margintop: '1em' }} > CPF * </label>
                            <input
                                id="cpf"
                                placeholder=""
                                value={cpf}
                                onChange={event => setCpf(event.target.value)}
                            />
                        </div>
                    </div>

                    <div style={{ display: 'row', width: 'fit-content' }}>
                        <div style={{ flexDirection: 'column', display: 'inline-flex' }}>
                            <label htmlFor="rua" style={{ margintop: '1em' }} > Rua * </label>
                            <input style={{ background: '#ddd' }}
                                disabled='true'
                                id="rua"
                                placeholder=""
                                value={rua}
                                onChange={event => setRua(event.target.value)}
                               
                            />
                        </div>

                        <div style={{ flexDirection: 'column', display: 'inline-flex', margin: '1em' }}>
                            <label htmlFor="cidade" style={{ margintop: '1em' }} > Cidade * </label>
                            <input style={{ background: '#ddd' }}
                             disabled='true'
                                id="cidade"
                                placeholder=""
                                value={cidade}
                                onChange={event => setCidade(event.target.value)}
                            />
                        </div>

                        <div style={{ flexDirection: 'column', display: 'inline-flex', margin: '1em' }}>
                            <label htmlFor="estado" style={{ margintop: '1em' }} > Estado * </label>
                            <input style={{ background: '#ddd' }}
                            disabled='true'
                                id="estado"
                                placeholder=""
                                value={estado}
                                onChange={event => setEstado(event.target.value)}
                            />
                        </div>
                    </div>

                    

                </div>

                

                <div style={{ display: 'row' }}>
                    <button className="btn" type="submit">SALVAR</button>

                    <button className="btn" style={{ marginLeft: '1em' }}  type='button'>
                        <Link to='/quartos-list' style={{ display: 'block', color: '#fff' }} >RESERVA</Link>
                    </button>

                    <button className="btn" style={{ marginLeft: '1em' }}  type='button'>
                        <Link to='/cliente-list' style={{ display: 'block', color: '#fff' }} >LISTAGEM</Link>
                    </button>

                    <button className="btn" style={{ marginLeft: '1em' }}  type='button'>
                        <Link to='/dashboard' style={{ display: 'block', color: '#fff' }} >VOLTAR</Link>
                    </button>
                </div>
            </form>

        </div>
    )
}