import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Dashboard from './pages/Dashboard';
import Login from './pages/Login';
import Quarto from './pages/Quarto';
import Reserva from './pages/Reserva';
import Cliente from './pages/Cliente';
import QuartoList from './pages/Quarto/list';
import ClienteList from './pages/Cliente/list';
import ReservaList from './pages/Reserva/list';

export default function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Login}/>
                <Route path="/dashboard" component={Dashboard}/>
                <Route path="/quartos" component={Quarto}/>
                <Route path="/quartos-list" component={QuartoList}/>
                <Route path="/cliente-list" component={ClienteList}/>
                <Route path="/reserva-list" component={ReservaList}/>
                <Route path="/reserva" component={Reserva}/>
                <Route path="/cliente" component={Cliente}/>
            </Switch>
        </BrowserRouter>
    );
}